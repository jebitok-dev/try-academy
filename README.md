# TRY Academy

![alt text](src/html/images/logo.png "TRY")


Our education system is BROKEN. Gating education behind money is a leading driver of wealth inequality and it’s shockingly ineffective at helping us create the lives we want for ourselves.

We’re going to fix it.

TRY Academy is the first worldwide community owned school and we’re going to PAY you to learn valuable real world skills. Software, science, math, farming, arts, music, history, conservation. All sorts of stuff. Whatever the community values. When one person improves themselves it helps ALL of us.

Student loan is a dirty word at TRY Academy.

## Why?

The internet has created many new free ways for people to learn. But in a lot of practical ways free education is still inaccessible. Usually it means that you’re mostly on your own.

Driven people will succeed anyway but learning is so much easier when you do it together. You get more practice when you have friends you can work with. It’s even better if you have access to knowledgeable people who you can ask for help. This is basically the premise of a university.

There are problems though. Our current learning models are expensive. Universities have big fancy buildings. Faculty. Processes. Bills. Shareholders. Allocating these resources is a difficult job that involves a lot of governance decisions. This all costs money.

Not to mention that FREE is still too expensive when you’re poor.

If college were magically free tomorrow the people who need it most still wouldn’t be able to pursue it. The opportunity costs are too high. Life is too expensive. Rich people literally don’t understand.

To pay these bills schools have turned students into customers. You enter their pipeline and you’re picked clean by financial vultures. BEFORE they’ve even taught you anything you’re agreeing to legal contracts written by multiple generations of lawyers.

Every lawyer who’s ever lived versus you: a kid who’s showing up at their doorstep because you don’t know stuff and just want to learn.

They “help” you take out loans from a bank or government and then you hand it over to them. Before you’ve learned even a single thing you’re chasing a debt they just created for you. Students shoulder all of the risk.

Maybe a good school would teach you not to fall for a trick like that.

Our colleges are designed to solve the college’s problems. They play a simple game. Get as many students to sign up for as much money as possible. The student’s problems are a secondary concern.

Paying the bills becomes a priority. Minimizing the resources that they allocate to learning maximizes their profit. Large classes become profit centers. Schools want to fire expensive and experienced teachers so bad that there are special rules about it. Unions become a counterbalancing force. But they also suck.

The systems morph to suck as much value as possible out of teachers and students and hand it to investors. No wonder it sucks. It’s designed to suck. As a student your personal goals are barely even on the table.

I want to aim higher than free education. I love teaching. I love learning. I want to make it simple and profitable to do both of these things.

At TRY Academy the instructors and students control the system. TRY is the governance token of TRY Academy. The wallets that hold it make the rules and control the treasury.

The TRY Academy app is a modern peer-to-peer learning management system and communication platform managed by smart contracts.

## How It Works
* A course is an NFT that has its own governance, treasury, and ownership stakes.

* A course can be bought, sold, and managed independent from the larger system.

* Actions we want to incentivize will increase a course’s rating. These will largely be tied to outside economic events. New technology allows us to decentralize these so that no one has to be in charge of enforcing it.

    Things like:
    * Fundraising
    * Recruiting
    * Marketing
    * Helping each other get employed

* We are a team and we are going to act like one. Our job is to make TRY valuable so that it’s meaningful when we earn it.

* The higher the course ranks the more TRY the students and instructors of that course earn.

* TRY is minted and given to courses based on their rating.

* The course can distribute as they see fit. According to rules in a smart contract. They decide what percent goes to students, instructors, curriculum developers, etc.

* The exact reward structure is a work in progress and will be community driven.

In future versions we will also interface with traditional LMS software so you can run an existing class this way.

The app itself is designed to be completely serverless. Peer-to-peer. No infrastructure. Ethereum, Chainlink, and IPFS will do most of the heavy lifting.

It will have all the tools we need to run a fully remote and online class. Video, audio, chat, etc. Group stuff. All peer to peer. The users will directly host the content. IPFS and Filecoin will make it easy for us to make things highly available.

It also makes it easy to fork and experiment with new versions without having to worry about infrastructure cost.
There’s no infrastructure to hold hostage. There’s no “YouTube” that can impose rules on us just because they’re hosting some videos. IPFS and Filecoin create new ways to decentralize the responsibility of keeping things online and we will utilize them extensively.

This doesn’t mean we’re going to do everything ourselves. The internet has so many existing resources available and there’s no reason to reinvent the wheel. Part of learning a craft is to learn how to use all the available tools. We will lean HEAVILY on outside resources. Especially at the beginning.

The TRY Academy software bootcamp will walk you through the design process we’re using to build “American Space Oligarch”. This is a fantasy sports game.

It’s better if you have no clue what fantasy sports are.

The dirty secret in software development is that most of the time you’re building stuff that other people want. Things your boss wants. At TRY Academy your "bosses" want you to build a fantasy sports app. Accept it.

We’ll use Chainlink to integrate with GitLab to reward tokens as you progress.

There will be checklists and you have to go through each and every step. Part of each checklist will be to get feedback from your peers. Anyone who helps give feedback will also get tokens. You’ll have to obsessively grade each other’s stuff.

You will get a TON of exposure to the exact techniques you are trying to learn. You’ll also be forced to communicate to both your peers and people who know more and less than you.

This is automated P2P learning with rewards.

The best way to learn is to teach. By the time you are done you will also be able to teach this course. Or you’re not done yet. It’s like martial arts. You’re either ready to proceed or you continue practicing and try again later. There’s no failing.

You’ll join a pod which is basically 4–5 people you’re grouping up with. You tie your hands to each other. As you all progress you all get tokens. You all need to finish each milestone as a group. You need to learn to work with a team. It’s a requirement.

Grouping up rewards SIGNIFICANTLY more tokens than doing it by yourself. You group up for individual milestones and exercises. Think of it like a dungeon raid in a role playing game. You’re not married necessarily.

At each big milestone you’ll present what you’ve done to a larger group. A presentation. You’re going to learn to talk. Talking is important. You can’t do well in interviews or at work if you can’t talk.

Once the instructor approves of your presentation you can move on to the next milestone. This will unlock new material. Tokens will be rewarded to the student, instructor, and to anyone else in their group.

The TRY Academy software itself will also be built using these same processes. It will serve as a starting point for graduates to copy as a base model for their own application ideas.

We’ll end up with a large group of trained blockchain developers who are personally invested in the system. I suspect it will work in practice like a tech incubator.

The course itself will be edited like Wikipedia basically. But tokens will be rewarded for contributions that successfully make it into the course. It’s possible there will also be a kind of royalty for this.

In the long run we’ll also have built in chat + video chat + stack overflow. It’ll be entirely self-contained. Everything to run a fully remote and online P2P class. In a lot of ways Slack and Discord are good UI inspirations. Ultimately this is a communication tool to facilitate group learning.

Students can join multiple classes. Eventually the classes will be configurable but for now we’re going with a simple base model. Our Model-T.

The reward system will be fully P2P. Still thinking through how this will work exactly but in a lot of ways this is going to feel like World of Warcraft and not a school. We’re all in the world together and we can all teach and learn from each other. Just like an MMORPG there will be a user driven economy that isn’t necessarily dictated to us. Also the classes and entire system will be easy to fork if you want to try a slightly different variation.

Eventually you get to the highest level — like earning a black belt — and you can do all the end-game material but it’s not like you suddenly morph into “teacher”. You’ve been teaching and learning the entire time. This also creates a framework for rewarding continued education. Like releasing an expansion pack that adds new higher levels.

If you’re interested please join our discord and help us build this.

[Live](https://tryuniversity.com)

[Discord](https://discord.gg/mH8yHuj)