import assert from 'assert'

import { getTestContainer } from "../test-inversify.config"
import { Student } from "../../src/dto/student"
import { StudentService } from '../../src/service/student-service'

describe('StudentService', async () => {

    let studentService: StudentService


    before('Before', async () => {

        let container = await getTestContainer()
        studentService = container.get(StudentService)

    })    

    it("should create and read a student", async () => {
        
        //Arrange
        let student:Student = {
            firstName: "Patrick",
            lastName: "Toner"
        }

        //Act
        await studentService.put("1", student)

        //Assert

        let savedStudent = await studentService.get("1")

        assert.equal(student.firstName, savedStudent.firstName)
        assert.equal(student.lastName, savedStudent.lastName)
        
    })


    it("should update an existing student", async () => {
        
        //Arrange
        let person:Student = {
            firstName: "John",
            lastName: "Davey"
        }

        //Act
        await studentService.put("1", person)

        //Assert

        let savedStudent = await studentService.get("1")

        assert.equal(person.firstName, savedStudent.firstName)
        assert.equal(person.lastName, savedStudent.lastName)
        
    })


    it("should get a list of students", async () => {

        //Act
        for (let i=0; i < 10; i++) {
            await studentService.put(i.toString(), { 
                firstName: `First${i.toString()}`, 
                lastName: `Last${i.toString()}` 
            })
        }

        //Get a couple of lists
        let list = await studentService.list(0,10)
        let list2 = await studentService.list(5,5)

        //Assert
        assert.equal(list.length, 10)
        assert.equal(list[9].firstName, "First9")

        assert.equal(list2.length, 5)
        assert.equal(list2[4].firstName, "First9")

    })


})
