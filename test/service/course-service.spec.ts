// import assert from 'assert'

// import { getTestContainer } from "../test-inversify.config"
// import { Course } from "../../src/dto/course"
// import { CourseService } from '../../src/service/course-service'

// describe('CourseService', async () => {

//     let courseService: CourseService


//     before('Before', async () => {

//         let container = await getTestContainer()
//         courseService = container.get(CourseService)

//     })    

//     it("should create and read a course", async () => {
        
//         //Arrange
//         let course:Course = {
//             founderWallet: "0xY",
//             name: "Fancy Networking"
//         }

//         //Act
//         await courseService.put("1", course)

//         //Assert

//         let savedCourse:Course = await courseService.get("1")

//         assert.equal(course.founderWallet, savedCourse.founderWallet)
//         assert.equal(course.name, savedCourse.name)
        
//     })


//     it("should update an existing course", async () => {
        
//         //Arrange
//         let course:Course = {
//             founderWallet: "0xZ",
//             name: "Fancy Networking 200"
//         }

//         //Act
//         await courseService.put("1", course)

//         //Assert

//         let savedCourse:Course = await courseService.get("1")

//         assert.equal(course.founderWallet, savedCourse.founderWallet)
//         assert.equal(course.name, savedCourse.name)
        
//     })


//     it("should get a list of courses", async () => {

//         //Act
//         for (let i=0; i < 10; i++) {
//             await courseService.put(i.toString(), { 
//                 name: `First${i.toString()}`
//             })
//         }

//         //Get a couple of lists
//         let list = await courseService.list(0,10)
//         let list2 = await courseService.list(5,5)

//         //Assert
//         assert.equal(list.length, 10)
//         assert.equal(list[9].name, "First9")

//         assert.equal(list2.length, 5)
//         assert.equal(list2[4].name, "First9")

//     })


// })
