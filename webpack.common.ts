import path from 'path'

const CopyPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const WebpackPwaManifest = require('webpack-pwa-manifest')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')


const babelLoader = {
  loader: 'babel-loader',
  options: {
    cacheDirectory: false,
    presets: ['@babel/preset-env']
  }
}

const framework7ComponentLoader = {
  loader: 'framework7-component-loader-spacemvc',
  options: {
    helpersPath: './src/template7-helpers-list.js',
    partialsPath: './src/pages/',
    partialsExt: '.f7p.html'
  }
}


export default {
  entry: './src/index.ts',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: ['ts-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        loader: ['style-loader', 'css-loader']
      },
      {
        test: /\.(png|jpe?g|gif|svg|eot|ttf|woff|woff2)$/,
        use: [
          'file-loader',
        ],
      },
      {
        test: /\.f7.html$/,
        use: [babelLoader, framework7ComponentLoader],
      }
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.tsx', '.ts'],
  },
  output: {
    filename: 'tryschool.js',
    library: "tryschool",
    path: path.resolve(__dirname, 'public')
  },
  plugins: [
    new CleanWebpackPlugin(),

    new HtmlWebpackPlugin({
      inject: false,
      title: 'TRY School | JUST TRY!',
      favicon: 'src/html/favicon.ico',
      template: 'src/html/index.html',
      filename: 'index.html'
    }),

    new WebpackPwaManifest({
      name: 'TRY School',
      short_name: 'TRY',
      description: ' We are a worldwide community owned school.',
      icons: [
        {
          src: path.resolve('src/html/apple-touch-icon.png'),
          size: '180x180'
        },
        {
          src: path.resolve('src/html/android-chrome-512x512.png'),
          size: '512x512' // you can also use the specifications pattern
        },
        {
          src: path.resolve('src/html/android-chrome-192x192.png'),
          size: '192x192'
        },
        {
          src: path.resolve('src/html/favicon-16x16.png'),
          size: '16x16'
        },
        {
          src: path.resolve('src/html/favicon-32x32.png'),
          size: '32x32'
        }
      ]
    })
  ]
}





