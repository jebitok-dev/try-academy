import { dao, MetastoreService, Initializable } from "space-mvc"
import { Course } from "../dto/course"

@dao()
class CourseDao implements Initializable {

    private store

    constructor(
        private metastoreService:MetastoreService
    ) {}

    async init() {
        this.store = this.metastoreService.getStore("course") 
    }

    async put(key:string, course:Course) {

        //Store the course DTO

        //Save the schema for the course

        return this.store.put(key, course)
    }

    async get(key:string): Promise<Course> {




        return this.store.get(key)
    }

    async list(offset?: number, limit?: number) : Promise<Course[]> {
        return this.store.list(offset, limit)
    }

}

export {
    CourseDao
}