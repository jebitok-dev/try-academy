import  { ModelView, controller, routeMap, RoutingService } from "space-mvc"


import HomeComponent from '../components/home/home.f7.html'
import HomePanelComponent from '../components/home/panel.f7.html'
import HomeWorkspacesComponent from '../components/home/workspaces.f7.html'


@controller()
class HomeController {

    constructor(
        private routingService:RoutingService
    ) {}

    @routeMap("/")
    async showIndex(): Promise<ModelView> {

        this.routingService.navigate({ path: "/home/workspaces" }, {}, "workspaces")
        this.routingService.navigate({ path: "/home/panel" }, {}, "channels")
        this.routingService.navigate({ path: "/home/main" }, {}, "main")

        return new ModelView(async () => {})
    }


    @routeMap("/home/panel")
    async showPanel(): Promise<ModelView> {
        
        return new ModelView(async () => {
        }, HomePanelComponent)

    }

    @routeMap("/home/main")
    async showMain(): Promise<ModelView> {
        
        return new ModelView(async () => {
        }, HomeComponent)

    }

}

export { HomeController }
