import  { ModelView, controller, routeMap, RoutingService } from "space-mvc"


import CourseCreateComponent from '../components/course/create.f7.html'
import CourseChatComponent from '../components/course/chat.f7.html'
import CourseChannelsComponent from '../components/course/channels.f7.html'


@controller()
class CourseController {

    constructor(
        private routingService:RoutingService
    ) {}

    @routeMap("/course/create")
    async showCreate(): Promise<ModelView> {
        
        return new ModelView(async () => {
        }, CourseCreateComponent)

    }

    @routeMap("/course/index")
    async showIndex(): Promise<ModelView> {
        
        this.routingService.navigate({ path: "/course/channels" }, {}, "channels")
        this.routingService.navigate({ path: "/course/chat" }, {}, "main")

        return new ModelView(async () => {})
    }

    @routeMap("/course/channels")
    async showChannels(): Promise<ModelView> {
        
        return new ModelView(async () => {
        }, CourseChannelsComponent)

    }

    @routeMap("/course/chat")
    async showChat(): Promise<ModelView> {
        
        return new ModelView(async () => {
        }, CourseChatComponent)

    }

}

export { CourseController }
