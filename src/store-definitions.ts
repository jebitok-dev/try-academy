import { StoreDefinition } from "space-mvc";

let mainStoreDefinitions: StoreDefinition[] = [
    {
        name: "student",
        type: "mfsstore",
        load: 100,
        options: {
            schema: {
                walletAddress: { unique: true },
                firstName: { unique: false },
                lastName: { unique: false }
            }
        }

    },
    {
        name: "course",
        type: "mfsstore",
        load: 100,
        options: {
            schema: {
                founderWallet: { unique: false },
                name: { unique: false }
            }
        }

    }


]




export {
    mainStoreDefinitions
}